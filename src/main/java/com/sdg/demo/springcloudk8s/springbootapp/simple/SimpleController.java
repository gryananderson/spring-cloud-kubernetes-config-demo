package com.sdg.demo.springcloudk8s.springbootapp.simple;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class SimpleController {

    private final SimpleConfiguration simpleConfiguration;

    private SimpleController(SimpleConfiguration simpleConfiguration) {
        this.simpleConfiguration = simpleConfiguration;
    }

    @GetMapping("/simple")
    public String getSimpleConfiguration() {
        return String.format("Bands=%s", simpleConfiguration.getBands());
    }

}
