package com.sdg.demo.springcloudk8s.springbootapp.feature;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties(prefix = "sdg.features")
public class FeatureConfiguration {

    private Boolean featureFlag;

    public Boolean isFeatureFlagEnabled() {
        return featureFlag;
    }

    public FeatureConfiguration setFeatureFlag(Boolean featureFlag) {
        this.featureFlag = featureFlag;
        return this;
    }
}
