package com.sdg.demo.springcloudk8s.springbootapp.secret;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties(prefix = "sdg.secret")
public class SecretConfiguration {

    private String username;
    private String password;

    public String getUsername() {
        return username;
    }

    public SecretConfiguration setUsername(String username) {
        this.username = username;
        return this;
    }

    public String getPassword() {
        return password;
    }

    public SecretConfiguration setPassword(String password) {
        this.password = password;
        return this;
    }
}
