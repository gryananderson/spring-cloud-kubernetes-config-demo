package com.sdg.demo.springcloudk8s.springbootapp.feature.service;

public interface FeatureService {
    String doStuff();
}
