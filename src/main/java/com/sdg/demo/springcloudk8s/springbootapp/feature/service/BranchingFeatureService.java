package com.sdg.demo.springcloudk8s.springbootapp.feature.service;

import com.sdg.demo.springcloudk8s.springbootapp.feature.FeatureConfiguration;
import org.springframework.stereotype.Component;

//@Component
public class BranchingFeatureService implements FeatureService {

    private final FeatureConfiguration featureConfiguration;

    public BranchingFeatureService(FeatureConfiguration featureConfiguration) {
        this.featureConfiguration = featureConfiguration;
    }

    @Override
    public String doStuff() {
        if (featureConfiguration.isFeatureFlagEnabled()) {
            return "Branching - Feature enabled";
        }
        return "Branching - Feature disabled";
    }
}
