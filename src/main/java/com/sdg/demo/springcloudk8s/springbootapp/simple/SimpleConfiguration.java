package com.sdg.demo.springcloudk8s.springbootapp.simple;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import java.util.List;

@Configuration
@ConfigurationProperties(prefix = "sdg.simple")
public class SimpleConfiguration {

    private List<String> bands;

    public List<String> getBands() {
        return bands;
    }

    public SimpleConfiguration setBands(List<String> bands) {
        this.bands = bands;
        return this;
    }

}
