package com.sdg.demo.springcloudk8s.springbootapp.feature.service;

import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Component;

@Component
@ConditionalOnProperty(name = "sdg.features.featureFlag", havingValue = "true")
public class FeatureEnabledService implements FeatureService {

    @Override
    public String doStuff() {
        return "Feature enabled";
    }
}
