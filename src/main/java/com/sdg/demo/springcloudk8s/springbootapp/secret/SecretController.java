package com.sdg.demo.springcloudk8s.springbootapp.secret;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class SecretController {

    private final SecretConfiguration secretConfiguration;

    private SecretController(SecretConfiguration secretConfiguration) {
        this.secretConfiguration = secretConfiguration;
    }

    @GetMapping("/secret")
    public String getSecretConfiguration() {
        return String.format("Username=%s, password=%s", secretConfiguration.getUsername(), secretConfiguration.getPassword());
    }

}
