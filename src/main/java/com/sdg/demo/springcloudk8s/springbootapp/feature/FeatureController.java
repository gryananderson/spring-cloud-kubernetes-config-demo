package com.sdg.demo.springcloudk8s.springbootapp.feature;

import com.sdg.demo.springcloudk8s.springbootapp.feature.service.FeatureService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class FeatureController {

    private final FeatureConfiguration featureConfiguration;
    private final FeatureService featureService;

    public FeatureController(FeatureConfiguration featureConfiguration, FeatureService featureService) {
        this.featureConfiguration = featureConfiguration;
        this.featureService = featureService;
    }

    @GetMapping("/feature")
    public String getFeatureConfiguration() {
        return String.format("FeatureFromConfig=%s, ResultFromService=%s", featureConfiguration.isFeatureFlagEnabled(), featureService.doStuff());
    }

}
