kubectl delete deployment spring-boot-app -n sdg-spring-cloud-k8s
docker build --no-cache -t spring-boot-app:latest -f docker/Dockerfile .
kubectl apply -f kubernetes/spring-boot-app-deployment.yml
kubectl apply -f kubernetes/spring-boot-app-service.yml